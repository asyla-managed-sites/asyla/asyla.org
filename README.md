# README

Basic getting started guide for this repo.  You will need this info if you wish to fully fully manage this site.

## Overview

This is predominately a website comprised of text files in [Markdown](https://commonmark.org/help/) format utilizing [Obsidian](https://obsidian.md/) for file and link management.  [MkDocs](https://www.mkdocs.org/) is a static site generator, along with the [mkdocs-material](https://github.com/squidfunk/mkdocs-material) theme, is used to compile the source files into HTML.  All of this is hosted (for free!) on [GitLab](on https://gitlab.com/) which also provides security checks and such.  DNS and WAF services are provided by [CloudFlare](https://www.cloudflare.com/) for a nominal fee.

All content is generated using [Obsidian](https://obsidian.md/) but a certain degree of technical skill is required to contribut to the repo much less when operating in a group setting.

Dont dispare if you find [Markdown](https://commonmark.org/help/) confusing, dont understand what [git](https://git-scm.com/) is much less how to use it, and are lost regarding [CI/CD](https://docs.gitlab.com/ee/ci/).  You can still contribute!  Just work with those who maintain the site to get your content published.

## Instructions

### GitLab tasks

In GitLab, setup a Personal (not Project) Access Token for the project/repo.  Dont loose this!

* in your profile, goto `Access Tokens`
* access would be `api` (this is TBD)
* save the access token (ie in Bitwarden)

**NOTE**: Using [Sourcetree](https://www.sourcetreeapp.com/) has proven a convienent way to manage local changes and is highly recommended.  It will need the URL for the repo, your user ID, and the GitLab Personal Access Token you created above.  You can instead use `git` but it is assumed you know what you are doing.

### Obsidian tasks

Download/install [Obsidian](https://obsidian.md/) on your computer.  The software is free (but please support them) and does not require admin rights (at least on MacOS).

**NOTE**: To make your life considerably easier, obtain a copy of the `.obsidian` directory from another user if there isnt an `obsidian.zip` file.  Place that within the Obsidian Vault directory which is the root of the website.  You can tweak things after the fact but there are a number of plugins and other settings which you will want to have from the beginning and this will make it go much easier.

Launch Obsidian

From the 'open another vault' menu (the first screen if there are no vaults), select 'Open folder as vault' and point it at the **Library subdirectory**.  ie: if the repo is located in `~/local/whatever/Documentation`, then point Obsidian at the `~/local/whatever/Documentation/Library` directory.  *Using any other location will break things.*

**IMPORTANT**: You will be prompted about enabling plugins.  Permit this.

**IMPORTANT**: Turn off restricted mode.  This will enable communitiy plugins.

At this point you should be able to start working

### Copying this repo

If you were using this site as a template for a new one, make sure you go through all the files/folders at the root leval and update everything for the new site.  Look closely, there are lots of little changes to make.  You can delete most of the Obsidian Vault's contents but leave the items needed to operate the site, the `_template` folder and `README.md`.

Within GitLab, will need to make sure the pipelines run (they wont on the initial commit).  Make sure the site is published.  Make sure the repo is public unauthenticated users can access it.

If you arent authenticated, Chrome is going to complain that the site is dangerous.  Go through the warning messages so you can actually see if the site is ok.

At this point, follow the GitLab's instructions in the repo for how to point your own domain name at the site.
