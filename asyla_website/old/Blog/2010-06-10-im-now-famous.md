---
layout: post
title: Im famous
tags:
- Ham Radio
- HPV
- Projects
status: publish
type: post
published: true
meta:
---
...well, no not really.  But my bike and I did appear in a [Make magazine video](http://blog.makezine.com/archive/2010/06/mobile_radios_at_hamvention_2010.html):

<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="462" height="278" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"><param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" /><param name="src" value="http://www.youtube.com/v/rn7y35hZoU8&amp;color1=0xb1b1b1&amp;color2=0xd0d0d0&amp;hl=en_US&amp;feature=player_detailpage&amp;fs=1" /><param name="allowfullscreen" value="true" /><embed type="application/x-shockwave-flash" width="462" height="278" src="http://www.youtube.com/v/rn7y35hZoU8&amp;color1=0xb1b1b1&amp;color2=0xd0d0d0&amp;hl=en_US&amp;feature=player_detailpage&amp;fs=1" allowscriptaccess="always" allowfullscreen="true"></embed></object>

My portion was at the Bicycle Mobile Hams of America ([BMHA](http://www.bmha-hams.org/)) forum at [Dayton Hamvention](http://www.hamvention.org/)
