---
layout: post
title: FWDMBBLR
tags:
- HPV
- Projects
status: publish
type: post
published: true
meta:
---
![](../images/FWDMBBLR.png)

This is a work in progress.  I want to have a "go fast" recumbent because my [Greenspeed](http://www.greenspeed.com.au/) GTR isnt built for sustained speed and now has way to much stuff on it anyways.

It is a front wheel drive, moving bottom bracket, low racer design with rear suspension and, if I can manage it, front suspension as well.

***

2010-06-13:  Currently waiting for the main body tube to come back from [Wagner](http://www.wagnercompanies.com/).

