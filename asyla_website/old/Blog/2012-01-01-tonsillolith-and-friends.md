---
layout: post
title: Tonsillolith and friends
tags:
- Health
status: publish
type: post
published: true
meta:
---
For as long as I can remember, Ive had regular occurrences of [tonsilloliths](http://en.wikipedia.org/wiki/Tonsillolith) (aka tonsil stones).  The only reason Im even bringing this up now is because last week I had my first one in about a year.

These things are more of an annoyance then anything.  From reading about it, tonsillolith can manifest in different ways.  In my case they generally get quite large in a half day or so.  Fortunately they can be popped out fairly easily and long ago I learned that if I dont do something about it within a day of it forming then Ill get sick.  The progression is a sore throat which then moves up into my sinuses which then becomes a full blown cold.  Fun.  I would have one form every now an again but it was at least once a month.  I just got into the habit of checking my tonsils on a regular basis and as long as I did then I wouldnt get sick.  The thing is, when I went Paleo at the beginning of 2011, this completely went away.  And I didnt even notice.  It was somewhere around Fall when I even realized.  Weird.

So by going Paleo I removed, among other things, all grains from my diet.  And last Tuesday I had a serving of white rice.  We were at a restaurant (Qdoba) and I completely forgot to tell the person no rice until it was too late.  Oh well.  Its white rice.  How bad can it be?  [Robb Wolf](http://robbwolf.com/) even has made statements to the effect that white rice is the least bad type.  Plus I was hungry which tends to make rationalizations like this possible.  Anyrate, it had formed by Wed.  Meanwhile, Im still dealing with the other after effects I get from grains:  acne and generally just not feeling quite right.  Generally if I get a low dose of soy or wheat, the "not quite right" feeling creeps up and subsides the same day.  Acne takes a little longer to show and pretty consistently forms the next day.  All of the major effects are generally out of my system within a day or two (not counting the healing time for the acne).  In this case from eating a scoop (~1C?) of rice, its still going after 5.

