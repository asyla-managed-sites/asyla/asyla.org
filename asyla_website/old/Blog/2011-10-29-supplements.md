---
layout: post
title: Supplements
tags:
- Health
status: publish
type: post
published: true
meta:
---
## Supplements ##
The topic of supplements has come up now and again so I thought I should post something about it.

I should mention at this point that Ive run across a fair number of articles citing research showing that large doses of vitamins/mineral are not good for you in the long run.  As well as large amounts of antioxidents, fruit extract, etc.  As always, its best to obtain everything from your diet and environment and, well, supplement the little bit that is missing.

Here is the list of what Im taking on a daily basis:

<div>
<ul>
	<li>ZMA</li>
	<li>Vitamin D</li>
	<li>NOW Super Enzymes</li>
	<li>Carlsons Fish Oil</li>
	<li>Glucosamine Chondroitin MSM</li>
	<li>Protein powder</li>
</ul>
</div>

You might notice, I *dont* have the ubiquitous multivitamin on the list.  Since, Im following the [Paleo](http://thepaleodiet.com/) diet, Im getting all the nutrients the multivitamin contains.  Since overdosing is bad, Ill just eliminate that from my diet.

### [ZMA](http://en.wikipedia.org/wiki/ZMA_%28supplement%29) ###

**Z**inc monomethionine [Aspartate](http://en.wikipedia.org/wiki/Aspartate) and **M**agnesium **A**spartate (ZMA).  Im taking this for a couple reasons and its primarily for the magnesium as a sleep aid.  It does seem to help somewhat and certainly doesnt hurt either.  Obviously I take this at night.

Whenever I have felt the onset of a cold coming on, Ive taken zinc acetate to lessen the severity and speed up the recovery time.  This was originally based on some research I ran across some years ago on the subject (ie, [article1](http://www.evernote.com/shard/s46/sh/5a6eb8b9-f675-4bff-b4c1-c55924493070/0c78ffa8952066a3efd9914e030c2067), [article2]((http://www.evernote.com/shard/s46/sh/4e5bc5da-019d-4174-a760-0f83981b1808/9d795f3a76cc3650ef51f5d37331a47a)).  Now its because I found it works quite well.  Unfortunately the products ([ColdEze](http://www.coldeeze.com/) and my preferred [Zincum](http://www.zicam.com/) that I found at the store tend to run somewhat low in their doesage rates.  Even worse, they are homeopathic.  As with anything and especially when dealing with homeopathy, make sure you *read and understand* what the ingredient list says.  Anyrate, at the beginning of the year, in my N=1 experiment I was taking ZMA when I managed to catch a cold.  I wasnt able to get to the store in time for my usual remedies but I still recovered quickly.  Nothing scientific of course but I think it helped.

Now that Ive been [Paleo](http://thepaleodiet.com/) for ~10 months now, Im pretty sure I get enough zinc in my diet.  Not sure about the magnesium since some paleo notables (ie [Robb Wolf](http://robbwolf.com/)) are supplementing with it themselves.  No way to tell without a blood test which I might do someday.  In the meantime, once I run out of ZMA, Ill likely switch over to [Natural Calm](http://www.petergillham.com) and see how I feel.

### Vitamin D ###
Im currently taking 2,000 units of this in the morning.  Main thing Ive noticed is that after taking this I started waking up easier.  Somewhere I ran across 10,000 units being about how much is produced by the body in a day.  There is also a good bit talking about min/max blood levels, the problems that occur if you go too far either way (ie: [article1](http://www.evernote.com/shard/s46/sh/137d4184-a71d-4196-bafc-08e4a3ebb86b/a30e61eabf8a8b8f63bac387968a690b), [article2](http://www.evernote.com/shard/s46/sh/6cc6c2f1-ac49-4d31-8603-3227290a2f3c/27ef538a482347c8525c17666ff0d235)) and etc.  Since I work at a desk and generally only see the sun on occasion and not having enough info, I started off at 10K units.  About a month later I backed off to 5000 units and never felt any difference.  A week or so after switching to 2000 units and Im having a bit harder time waking up in the morning but thats likely more because I havnt been getting to bed early enough.  I really should do something about the sleep and lack of sun.

Oh, also, I read somewhere that fish oil blocks vitamin D absorbtion which is why I take this in the morning.

### [NOW](http://www.nowfoods.com/) Super Enzymes ###
I got this from Robb Wolf's podcast.  Have to say that I was skeptical at first but unlike any other supplement, the effects are fast and noticeable.  Robb's instructions are to increase the dosage by 1 pill per meal until you feel a "warming" sensation.  Once you reach that point, back off by one pill.  Dont exceed 6 in a meal.  Dont drink a ton of water during the meal because it will be diluted.  I also suggest limiting yourself to, say, 1 pill if you arent eating meat but your mileage may vary.

### Fish Oil ###
I take this one at night.  Lately Ive been hearing that the fermented version is supposed to be better, easier to absorb, etc but I havnt really looked into it yet.  Thing is, I have some mixed opinions about the whole topic.  The primary reason to take fish oil is for the [Omega-3 fatty acids](http://en.wikipedia.org/wiki/Omega-3_fatty_acid).  We dont get enough.  Or rather, we get FAR too many [Omega-6 fatty acids](http://en.wikipedia.org/wiki/Omega-6_fatty_acid).  The discussion about why is far outside the scope of this.  Go read/listen about the paleo diet for a while if you care about the details.  Anyrate, by supplementing with fish oil, we arent fixing the problem.  Yeah, the ratios are getting closer to balanced but the total quantities are way out of wack.  Better would be to remove the problem in the first place.  However, grass fed meat isnt exactly stocked at the grocery here and the other sources are a bit more difficult (and more expensive).  Course, fish oil isnt exactly cheap either.  While Im pondering this delemma, Im taking a fairly low dosage.

### Glucosamine Chondroitin MSM ###
I started taking this on a regular basis because my joints are creaky and 2 doctors suggested that I should do it.  So I took the hint.  I find its only a minor improvement if at all.  I might stop for a month when I run out to see if I notice any differences.  Currently taking [Osteo Bi-Flex](http://www.osteobiflex.com/) since it is easy to find and seems to have the highest concentration per pill (read cheapest) but I kinda like the one from [KAL](http://www.nutracorp.com/) because it doesnt have the huge dose of sodium like the others.

### Protein powder ###
Almost entirely based on an [article](http://www.evernote.com/shard/s46/sh/f8e2a55a-d240-42e8-9455-9c0a901814c6/e27d7e251972bac608851b76d490b5ac%20) I read, Ive been taking 30g of protein powder after workouts for the leucine.  If anyone is interested, Im purchasing my own custom blend from [True Protein](http://www.trueprotein.com/) which is made up of the following:
<ul>
	<li>25% Hydrolyzed Casein (bitter)</li>
	<li>75% Hydrolyzed Whey Protein Super Grade (chalky)</li>
</ul>
The casein is new this time around.  This is another N=1 experiment but overall I do feel that the protein it has been helping.  Ill keep tracking and see how I feel good enough about it to buy more when I run out of this batch.

