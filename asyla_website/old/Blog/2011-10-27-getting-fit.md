---
layout: post
title: Getting Fit
tags:
- Health
status: publish
type: post
published: true
meta:
  _edit_last: '1'
  sfw_comment_form_password: P1flDz1R0HTz
---
This article was originally posted by Ryan Atkins who runs [Crossfit Milwaukee](http://www.crossfitmilwaukee.com/) on his blog at [strongrealizations.blogspot.com](http://strongrealizations.blogspot.com/).  I thought this might be a good starting point for further posts later on so Ive reproduced it here:

My goal was to loose weight

I know Robb Wolf says to throw out the scale but I like numbers.  So I give you a chart instead:

![](../images/11.png)

The above weight history goes back into high school (155lb) to show my climb into obesity that was only marred by the occasional attempt to loose weight.  There was no real exercise to speak of but I was always a bit of a cyclist.  Somewhere around 1999 I started commuting to work and was averaging 1800-2000 miles a year.

The cluster of dots in 2000-2002 was the result of my first attempt to loose weight.  My methodology at the time was to exercise more.  A group of us would use the weight room in the building and would climb stairs.  No real structure.  Needless to say it didn’t last long and didn’t really change anything.

Somewhere around 2003 is when my weight broke 200lbs and cycling started to become difficult.  Each year after that I would have more and more problems such as chafing.

I think it was 2007 when my left knee sustained an early season cycling injury which prevented me from riding the rest of the year.  At the time I blamed the new set of pedals.  Now I don’t believe that they were at fault but was the final straw.  From that point on, my knee was a constant problem.  By 2009 both knees were almost constantly hurting.  I couldn’t goto a movie without my knees seizing up.  Airplane rides were murder.

![](../images/21.png)

It wasn’t until 2009 that I made my next attempt and was when I bought a used Concept2 rower that fall.  I saw a loss of weight for a little bit but my eating habits soon took care of that.  Weight overall was still climbing.  When I was researching about rowing is when I first heard about CrossFit but I didn’t really pay too much attention at the time.

![](../images/3.png)

2010 was a really bad year at work because we were acquired Oct 1, 2009 and my co-worker wisely found other employment in March.  Hours, workload, stress and weight went through the roof.  I barely even saw the bike much less rode it.  It was June when my weight hit 220lbs with no signs of stopping.  Interestingly enough even at this point some people still didn’t believe I was overweight.

Since I had no real control over anything else, I decided to loose weight.  I now know there are far better ways to go about that but Ill outline what I did starting June:

First, I decided to eliminate 500 Cal/day.  No, I wasn’t weighing/measuring my food or anything.  It was simply a matter of eating less of everything.  Since I knew that 1lb fat is 3500 Cal, this should work.  And it did.  Sorta.

At some point I ran across a discussion about some research about the addictive nature of sugar.  It really hit home as I have absolutely no self-control when it comes to anything sweet.  So I decided to treat my sugar addiction and went cold-turkey.  That was a rough couple of weeks but it got easier as the cravings went away.  This also helped my weight loss but not too much.

What made the really big difference was the idea that I should actually by hungry before I eat a meal.  I started making it a point of eating 3 meals a day but I would only eat just enough to make it through till the next meal.  This was <strong>quite</strong> effective and the weight was just falling off.  Of course, the body goes into starvation mode and so you have to keep eating <strong>less</strong> to continue this process.  Near the end, I was basically eating snacks to tide me over to the next meal but interestingly hunger levels weren’t really all that bad.  Somewhere in the middle of this phase I noticed that vegetables and such started tasting better and so I started gravitating my diet in that direction.

Work started easing up slightly late Nov, 2009.  Now that I had gotten my weight down to a more manageable ~178lbs, I knew I needed to do <em>something</em> or it will just go back so I started rowing again.  Meanwhile I also started researching fitness programs.  I knew the globo gyms weren’t the answer but I didn’t know what was.  At some point I recalled CrossFit again and so focused my full attention in that direction.  It looked tough but it lined up with everything that I was interested in.  Did a quick search and found that there was a local gym not too far from work.  After building up my nerve, I signed up for the free Sat class on Dec 4.

That class nearly killed me.  I was also hooked.  I spent the rest of the month reading, researching, and generally trying to apply what I learned from the class

Onramp started Jan 10, 2010 and I was ~184lbs.  The benchmark WOD (shuttle runs, air squats, push-ups, sit-ups) nearly killed me.  I spent the entire month in pain and coughing.  But I was improving and enjoying every minute of it.

Onramp introduced the Paleo diet and so I started reading about that.   I immediately cut out wheat from my diet and promptly went into withdrawal.   Cutting out sugar was easy.  The weight loss was easy.  I was dreaming about bread.  Just the thought of bread would cause cravings.  This lasted for a couple months.  Meanwhile I was refining my diet more and more.  I was mainly eating low density carbs with some protein.

My goal became to loose fat.  And perhaps not die after a workout.

By the end of the onramp, I knocked 1:29 off my time.  Signed up for unlimited and have been attending an average of 4.3 days/week.  For my 6 month anniversary, I knocked yet another 1:06 off my benchmark time.

![](../images/41.png)

By the March 19, 2011, my weight had stabilized at 167lbs.  I had lost ~55lbs.  Went from a tight 38” waist, down to a comfortable 32”.  Went through 2 belts.  I was pretty consistent about 1” off the waist per 10lb lost.  I was now the same clothing size that I was in high school but weighed 10lb more and was stronger.  My mother was kind enough to say I looked like I had consumption.  (*sigh*)  Ok, I admit the old clothes were a bit baggy but my mid March I finally had a body composition that I wanted for for the first time in my life.

![](../images/5.png)

Since my weight stabilized for a week or so and good enough is never good enough, I started wondering what % fat I was at and if I could improve it any.  So more research, some learning, some practice and after some months eventually started to get some consistent numbers.

![](../images/6.png)

Note that with such things, since the data points are calculated, the key is to utilize a consistent and repeatable testing methodology.  The numbers may be inaccurate but they all should have roughly the same amount of inaccuracy.  The nice thing is, this work showed me what I already knew from me results in the WoDs.  I was building muscle, loosing fat but not really changing weight.  I should also point out that the “wild” fluxuations in % fat are more due to the scale of the chart but are generally accurate in the 2<sup>nd</sup> half.  Early on its from me dialing in my diet.  There was the incident of a really long car trip, a giant bag of almonds and an even larger bag of jerky in July.  And then it was the yo-yo affect of me behaving one week and living off fruit the next

By Jul, I was starting to reevaluate my diet and fitness goals again mostly because I simply need more muscle and Im impatient.  I determined that I wasn’t eating enough protein, was getting virtually no fat and generally was living off of a mountain of greens at each meal.  It was time to make some changes.  I tinkered a bit and came up with a plan late Aug.  I switched over to a lot of protein with a more reasonable amount of fat and switched over to high density carbs.  I also cut back the fruit to one piece or less a day.  Finally, I started taking protein powder (specifically one high in Leucine) after workouts.  By the way, after this many months of Paleo, I find that plain squash actually tastes good.  The Paleo challenge was announced soon after this decision (good timing really).

Starting Sept, my goal changed to gain muscle for ~12 weeks.

Except there was one (major) problem.  My job.  Fortunately, I found a new one and started on Oct 1.    However, one word of advice:  DON’T try to do a Paleo challenge while changing jobs!  I’m sure you can easily pick out in the graph which 3 weeks were affected.

![](../images/7.png)

Despite the diet disruptions, the lines are trending in the right directions and I’ve gained ~3.5lbs lean weight in ~2months which is considerably faster then my estimations for earlier.

Here are my before and after pictures for the challenge.  During this time, I was doing the Starting Strength Advance Novice Program:

9/3/2011

![](../images/8.png)

![](../images/9.png)

10/9/2011

![](../images/10.png)

![](../images/111.png)

Ok, I know, not much difference.  But trust me when I say this is incredibly different from a year ago.  Or even from January.  Too bad I didn’t take pictures back then.

In March I generally had everyone gang up on me and forced me to get a regular doctor.  He ordered up a blood test:

<table style="width: 441px;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Date</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center"><strong>Units</strong></p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center"><strong>Range</strong></p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center"><strong>3/19/11</strong></p>
</td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center"><strong>8/27/11</strong></p>
</td>
<td valign="bottom" nowrap="nowrap" width="65">
<p align="center"><strong>10/8/11</strong></p>
</td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Fasting Status</strong></td>
<td valign="bottom" nowrap="nowrap" width="45"></td>
<td valign="bottom" nowrap="nowrap" width="54"></td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">TRUE</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">FALSE</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65">
<p align="center">TRUE</p>
</td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Glucose</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">mg/dL</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">65-100</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">76</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47"></td>
<td valign="bottom" nowrap="nowrap" width="65"></td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Hemoglobin A1C</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">%</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">3.0-6.0</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51"></td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">5.3%</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65"></td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>BUN</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">mg/dL</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">9-25</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">15</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">17</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65"></td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Creatinine</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">mg/dL</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">.45-1.26</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">1.18</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">1.4</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65"></td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Alkaline Phosphatase</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">U/L</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">28-126</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">65</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">81</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65"></td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Total Bilirubin</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">mg/dL</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">.2-1.3</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">0.6</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">0.5</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65"></td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>SGOT (AST)</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">U/L</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">14-59</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">33</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">44</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65"></td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>SGPT (ALT)</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">U/L</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">9-72</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">22</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">32</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65"></td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>GGT (GGTP)</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">U/L</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">2-65</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51"></td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">9</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65"></td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Total Protein</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">g/dL</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">6.3-8.3</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">7.3</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">7.4</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65"></td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Albumin</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">g/dL</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">3.5-5.0</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">4.6</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">4.8</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65"></td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Globulin</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">g/dL</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">1.0-4.0</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51"></td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">2.6</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65"></td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Triglycerides</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">mg/dL</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">35-149</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">80</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">70</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65">
<p align="center">48</p>
</td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Cholesterol</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">mg/dL</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">100-199</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">214</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">194</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65">
<p align="center">188</p>
</td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Cholesterol - LDL (calculated)</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">mg/dL</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">0-99</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">153</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">119</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65">
<p align="center">123</p>
</td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Cholesterol - HDL</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">mg/dL</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">40-200</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">45</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">61</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65">
<p align="center">55</p>
</td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Cholesterol - Non-HDL (calculated)</strong></td>
<td valign="bottom" nowrap="nowrap" width="45">
<p align="center">mg/dL</p>
</td>
<td valign="bottom" nowrap="nowrap" width="54">
<p align="center">0-190</p>
</td>
<td valign="bottom" nowrap="nowrap" width="51">
<p align="center">169</p>
</td>
<td valign="bottom" nowrap="nowrap" width="47"></td>
<td valign="bottom" nowrap="nowrap" width="65"></td>
</tr>
<tr>
<td valign="bottom" nowrap="nowrap" width="179"><strong>Cholesterol/HDL Cholesterol Ratio</strong></td>
<td valign="bottom" nowrap="nowrap" width="45"></td>
<td valign="bottom" nowrap="nowrap" width="54"></td>
<td valign="bottom" nowrap="nowrap" width="51"></td>
<td valign="bottom" nowrap="nowrap" width="47">
<p align="center">3.2</p>
</td>
<td valign="bottom" nowrap="nowrap" width="65"></td>
</tr>
</tbody>
</table>

Needless to say, he wasn’t happy with my cholesterol and promptly started talking about medication.  I politely declined so he scheduled a 6 month followup to see how I was doing.  Im pretty sure the intent was to build a case for putting me on meds but I wanted to see what the Paleo diet would do for me.  As you can see from the chart, Robb Wolf is telling the truth.  The funny thing is that my doctor, who was quite impressed with the improvement, was very insistent that I should eat oatmeal.  I politely declined that too.  At least he stopped trying to medicate me.  I think Ill aim for March to get another test done.  Perhaps Ill get them to do more then just cholesterol too.

Im curious to see where Ill be in Jan…..
