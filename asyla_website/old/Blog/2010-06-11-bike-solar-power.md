---
layout: post
title: Bike Solar Power
tags:
- HPV
- Projects
status: publish
type: post
published: true
meta:
---
Quick picture of the bike with a 25W flexible solar panel.  It stays put nicely using a velcro-like fastener on each of the corners.

![](../images/2009_07_19_13_49_12_IMG_6147.JPG)
