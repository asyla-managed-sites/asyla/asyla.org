---
layout: post
title: Under Seat Box v2
tags:
- HPV
- Projects
status: publish
type: post
published: true
meta:
---
The Greenspeed GTR has a good bit of empty space under the seat.  I tend to haul a lot of stuff and the pictures below are of the latest box Ive made:

![](../images/2009_07_19_14_15_36_IMG_6172.JPG)
![](../images/2009_07_19_13_52_03_IMG_6152.JPG)
![](../images/2009_07_19_13_52_33_IMG_6153.JPG)
![](../images/2009_07_19_13_52_58_IMG_6154.JPG)
![](../images/2009_07_19_13_54_16_IMG_6155.JPG)
![](../images/2009_07_19_13_56_57_IMG_6156.JPG)
![](../images/2009_07_19_13_57_23_IMG_6157.JPG)
