---
layout: post
title: Big Panniers
tags:
- HPV
- Projects
status: publish
type: post
published: true
meta:
---
Here is my 2<sup>nd</sup> pannier design spurred on by the success of my first design and my renewed interest in bike touring:

![](../images/2009_07_19_13_58_56_IMG_6159.JPG)

I only have pictures of the right one but its really a matched set.  Like the earlier design, this is using the [Arkel](http://www.arkel-od.com/) mounting system.

![](../images/2009_07_19_13_59_06_IMG_6160.JPG)

This picture provides a good view of the shape of this bag.  Notice the angle of the front (left side of the picture).  Since this is intended for use on a recumbent, I didnt have to worry about my feet hitting so I sloped it forward and got more room and somewhat better aerodynamics.

![](../images/2009_07_19_13_59_25_IMG_6161.JPG)

The construction is pretty similar to my [2010-06-11-pannier](2010-06-11-pannier.md) but there are some subtle differences.  First, the coroplast backing is not replaceable which is probably a mistake.  Second, if you notice, the bag is holding its shape without anything inside.  The Cordera outer shell is actually a sleeve which has a *really* fine fiberglass/epoxy sheet inside.

![](../images/2009_07_19_14_01_08_IMG_6162.JPG)

I like pockets

![](../images/2009_07_19_14_01_31_IMG_6163.JPG)

These panniers do a fairly good job but were somewhat expensive and difficult to make.  I like these bags but I couldnt help but think that I could still do better.
