---
layout: post
title: Camping Box
tags:
- HPV
- Projects
status: publish
type: post
published: true
meta:
---
Pictured here on the back of my bike (trike) is the latest evolution of my coroplast panniers:

![](../images/2009_07_19_13_48_55_IMG_6146.JPG)

Probably the biggest inspiration to this was a comment in one of the [Greenspeed](http://www.greenspeed.com.au/) newsletters a few years back.  As I recall, they were experimenting with utility designs and had completely eliminated the metal rack and replaced it with a coroplast box.  I liked the idea but didnt want to eliminate the rack (its too useful) and wanted to try putting *everything* (tent and all) inside the box.

One of the [GMRBC](http://www.gmrbc.org/) club members has access to a computer controlled box cutter and was kind enough to cut this out for  me.

Here is a picture of the insides.  To mount it to the rack, I made a ubolt out of a piece of all-thread.  Wing nuts are threaded on from inside.  A piece of string is used to keep the lid from falling open.  I have a piece of nylon window screen glued on to act as a pocket.

The left side is deep enough to hold the tent, sleeping bag, clothes, etc.  The water bladder, rain gear, etc lives in the top layer.

![](../images/2009_07_19_13_49_33_IMG_6148.JPG)

Good view of one of the front storage boxes here.  These are good for snacks and such but are a tad too high for easy access while riding.  I left holes in the bottom so they dont collect water.  And they nicely hold a quart container of blueberries....

![](../images/2009_07_19_13_49_46_IMG_6149.JPG)

You may have wondered why the right side isnt accessible from top.  Its because I have a kitchen.  Or at least a cupboard.  Pots, pans, stove, fuel, food, etc goes in here.

![](../images/2009_07_19_13_50_36_IMG_6151.JPG)

Back view here.  I hadnt thought about it at the time but that large flat surface is a huge energy sink.  The front isnt that great either.  I really need to design a more aerodynamic version

![](../images/2009_07_19_13_50_02_IMG_6150.JPG)

I originally intended this to be a bit deeper then it is but discovered that it interfered with my derailleur.  Ended up chopping off a couple inches from the whole thing.
