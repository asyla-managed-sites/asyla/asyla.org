---
layout: post
title: Guidelines
tags:
- Health
status: publish
type: post
published: true
meta:
---
## Some daily recommendations ##

### SLEEP ###

First and foremost, get 7-9 hours of sleep.  In a completely dark and quiet room. Completely dark means just that.  Get rid of all those lights and close the curtains.  If you can see your hands its too bright.  Relax prior to going to bed.  Try to wake without needing an alarm.

### EAT ###
I
n short, veggies fill you up.  Fat removes hunger.  Protein is needed to build muscle.  Muscle burns calories.

For protein, you will want ~1lb of lean meat, eggs or fish per day.  Preferably grass fed/free range.  Organ meat (ie liver) is especially good.  Don’t be afraid of fat.  It provides the satiety signal in your brain and is vital for good health.  Use coconut oil and olive oil.   Don’t forget to eat lots of varied veggies.  Drink plenty of water.  [Penzey's](http://www.penzeys.com/) is your friend and they are local.  Visit regularly.

Buy quality food.  In general it is safe to assume anything with a food label is likely bad for you.  Do not assume that the food label lists all ingredients and provides no indication of what has been done during processing.  The fact that it has been processed should be the first warning to avoid a product.  That it is called a product should be second.

Know what you are eating.  Cook your own food and take control of your diet.  The longer you eat this way the better whole foods taste.  Correspondingly, processed food tastes worse.

Resources:

[http://5by5.tv/paleo/](http://5by5.tv/paleo/)

This is a great introduction to Paleo and related topics.  Start with episode 1 and work your way forward.  Lots of good information here.

[http://www.everydaypaleo.com/](http://www.everydaypaleo.com/)

Want recipe ideas?  Want to know what it is like to live with the Paleo diet and raise a family?  This is the place to look.

[http://www.robbwolf.com/](http://www.robbwolf.com/)

Want to learn about Paleo in more detail?   Robb Wolf is a good resource.

If your goal is to loose weight, follow the above plan and make it a point to be hungry for the next meal.  If your goal is to gain muscle, eat enough that you aren’t.  The best strategy for many would be to build muscle first which will make loosing fat far easier.

Don’t forget that the amount of fat you consume will directly control how quickly you get hungry.  A great experiment to try:  For breakfast, eat only 1 sweet potato and 3 hardboiled eggs.  Note what time you get hungry prior to lunch (for me, 3hrs later).  Next day, eat the same thing with 1Tbs of coconut oil (110cal).  Note when (if?) you get hungry.  If you really wish to experiment, add the same number of calories worth of plain sugar and note the time.  It should be the shortest of all.

### EXERCISE ###

Mobility is incredibly important.  Without a full range of motion you are setting yourself up for injury.  Find what is lacking and work on it at least once a week.  Avoid things that limit mobility.  Straighten that back.  Get up from your desk at least once an hour.  Sleep on your back with a flat pillow, etc.

Be consistent with your program.  You will never improve anything if you don’t show up but hey its your life.

You probably aren’t strong enough so do something about it by lifting heavy things often.  Done fear the barbell.  Respect it and then own it.  Learn how to do the Olympic lifts <em>correctly</em>.  This takes time so be patient but is well worth the effort.  Do them often as part of your workout.  Focus on the quality and range of the movement before even thinking about the time it takes to do a workout.  If you cant do the motion correctly, ask for help, go slower and use a lighter weight.  There is no shame in this.  You will improve and that’s better then doing something stupid and hurting yourself.

Track your progress!!  Write everything down in a notebook.  You cant improve if you don’t know what you did.  If you aren’t improving, figure out what you are doing wrong.  Ask for help as needed.  After a certain point, the beginner gains will taper off.  Don’t be discouraged as this is to be expected.  That’s when the real training begins.

Learn your limits.  No not those.  Your head is just messing with you.  I mean your physical limits.  If you think you cant do something then you wont.  That has nothing to do with whether or not you are actually physically capable of it.  Once you know your physical limitations, ignore them and go find our what your genetic limitations are.

Maintain focus.  There are many fun and cool things out there.  CrossFit dabbles in many of them.  If you choose to go exploring, work out a plan and STICK TO IT for at least a couple months.  Randomly jumping around to the next cool thing wont do you any favors.

Thoughts about CrossFit MetCons:  If you wish to have good endurance and maintain a lighter build, go with lighter weights and go really fast.  If you are more interested in strength, go heavier and slower.  However, if you are already fairly light/thin, do yourself a favor and first focus on strength.  The rest will come easier.

### RECOVER ###

Less is more.  First you need to learn what that means.  Then you need to learn to actually live it.  This applies to all aspects of your life but as a starting point, remove the stress.

You can not recover when stressed.  You also will NOT recover when chronically fatigued.

If you are injured, then you need to recover!  Obvious huh?  Apparently not.  That means you need to actually focus on recovering and not continuing to make things worse.  Don’t do something that you will regret for the rest of your life just because you got caught up in the moment.

### EVERYTHING ELSE ###

A positive attitude goes a long way to improve quality of life.  Always strive to improve yourself.  Be curious, learn something, practice a new skill, expand your horizons, etc but most of all, LIVE and HAVE FUN!
